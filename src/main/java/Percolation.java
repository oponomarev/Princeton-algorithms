import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

/**
 * @Author Oleg Ponomarev
 * @since 16.01.2017
 */
public class Percolation {
    private int size;
    private byte[] grid;
    private int numberOfOpenedSites;
    private WeightedQuickUnionUF weightedQuickUnionUF;
    private WeightedQuickUnionUF backwash;

    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("n must be greater than zero");
        }
        this.size = n;
//        n + 2 because there is a virtual top and a virtual bottom
        grid = new byte[n * n + 2];
        numberOfOpenedSites = 0;
        weightedQuickUnionUF = new WeightedQuickUnionUF(n * n + 2);
        backwash = new WeightedQuickUnionUF(n * n + 1); // without virtual bottom
    }

    public void open(int row, int column) {
        validateIndexes(row, column);
        if (isOpen(row, column)) {
            return;
        }
        grid[getCountingNumber(row, column)] = 1;
        numberOfOpenedSites++;
//        connect with virtual top if in the first row
        if (row == 1) {
            weightedQuickUnionUF.union(0, getCountingNumber(row, column));
            backwash.union(0, getCountingNumber(row, column));
        }
//        connect with virtual bottom if in the last row
        if (row == size) {
            weightedQuickUnionUF.union(size * size + 1, getCountingNumber(row, column));
        }
//        connect with neighbors if they are opened
        if (row > 1 && isOpen(row - 1, column)) {
            weightedQuickUnionUF.union(getCountingNumber(row, column), getCountingNumber(row - 1, column));
            backwash.union(getCountingNumber(row, column), getCountingNumber(row - 1, column));
        }
        if (row < size && isOpen(row + 1, column)) {
            weightedQuickUnionUF.union(getCountingNumber(row, column), getCountingNumber(row + 1, column));
            backwash.union(getCountingNumber(row, column), getCountingNumber(row + 1, column));
        }
        if (column > 1 && isOpen(row, column - 1)) {
            weightedQuickUnionUF.union(getCountingNumber(row, column), getCountingNumber(row, column - 1));
            backwash.union(getCountingNumber(row, column), getCountingNumber(row, column - 1));
        }
        if (column < size && isOpen(row, column + 1)) {
            weightedQuickUnionUF.union(getCountingNumber(row, column), getCountingNumber(row, column + 1));
            backwash.union(getCountingNumber(row, column), getCountingNumber(row, column + 1));
        }
    }

    public boolean isOpen(int row, int column) {
        validateIndexes(row, column);
        return (grid[getCountingNumber(row, column)] == 1);
    }

    public boolean isFull(int row, int column) {
        validateIndexes(row, column);
        return (isOpen(row, column) && backwash.connected(0, getCountingNumber(row, column)));
    }

    public int numberOfOpenSites() {
        return numberOfOpenedSites;
    }

    public boolean percolates() {
        return weightedQuickUnionUF.connected(0, size * size + 1);
    }

    private void validateIndexes(int row, int column) {
        if (row < 1 || row > size || column < 1 || column > size) {
            throw new IndexOutOfBoundsException("invalid row or column");
        }
    }

    private int getCountingNumber(int row, int column) {
        return (row - 1) * size + column;
    }

    public static void main(String[] args) {
        int size = StdIn.readInt();
        Percolation percolation = new Percolation(size);
        while (!percolation.percolates()) {
            percolation.open(StdRandom.uniform(1, size + 1), StdRandom.uniform(1, size + 1));
        }
        StdOut.println(percolation.numberOfOpenSites() * 1.0 / (size * size));
    }
}
