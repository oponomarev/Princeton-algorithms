import edu.princeton.cs.algs4.Queue;

import java.util.Arrays;

/**
 * @author Oleg Ponomarev
 * @since 07.02.2017
 */
public final class Board {
    private static final int OFFSET = 1;
    private static final int EMPTY_CELL = 0;
    private char[] blocks;
    private int dimension;

    private Board(Board that) {
        this.blocks = Arrays.copyOf(that.blocks, that.blocks.length);
        this.dimension = that.dimension;
    }

    public Board(int[][] blocks) {
        int emptyCellCount = 0;
        this.dimension = blocks.length;
        this.blocks = new char[dimension * dimension + OFFSET];
        for (int i = 0; i < blocks.length; i++) {
            for (int j = 0; j < blocks[i].length; j++) {
                if (blocks[i][j] < 0 || blocks[i][j] > Character.MAX_VALUE) {
                    throw new IllegalArgumentException("char exceeded");
                }
                if (blocks[i][j] == EMPTY_CELL) {
                    emptyCellCount++;
                }
                this.blocks[i * dimension + j + OFFSET] = (char) blocks[i][j];
            }
        }
        if (emptyCellCount != 1) {
            throw new IllegalArgumentException("There is no empty cell in tne input or there are several ones");
        }
    }

    public int dimension() {
        return dimension;
    }

    public int hamming() {
        int count = 0;
        for (int i = OFFSET; i < blocks.length; i++) {
            if (blocks[i] != i && blocks[i] != EMPTY_CELL) {
                count++;
            }
        }
        return count;
    }

    public int manhattan() {
        int count = 0;
        for (int i = OFFSET; i < blocks.length; i++) {
            if (blocks[i] == EMPTY_CELL) {
                continue;
            }
            int currentRow = (i - OFFSET) / dimension;
            int currentColumn = (i - OFFSET) % dimension;
            int targetRow = (blocks[i] - OFFSET) / dimension;
            int targetColumn = (blocks[i] - OFFSET) % dimension;
            count += Math.abs(currentRow - targetRow) + Math.abs(currentColumn - targetColumn);
        }
        return count;
    }

    public boolean isGoal() {
        for (int i = OFFSET; i < blocks.length; i++) {
            if (blocks[i] != i && blocks[i] != EMPTY_CELL) {
                return false;
            }
        }
        return true;
    }

    public Board twin() {
        Board twin = new Board(this);
        if (blocks[OFFSET] != EMPTY_CELL) {
            if (blocks[OFFSET + 1] != EMPTY_CELL) {
                swap(twin.blocks, OFFSET, OFFSET + 1);
                return twin;
            } else {
                swap(twin.blocks, OFFSET, OFFSET + dimension);
                return twin;
            }
        } else {
            swap(twin.blocks, OFFSET + 1, OFFSET + 2);
            return twin;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Board board = (Board) o;

        return dimension == board.dimension && Arrays.equals(blocks, board.blocks);
    }

    public Iterable<Board> neighbors() {
        Queue<Board> boardQueue = new Queue<>();
        int i = OFFSET;
        while (blocks[i] != EMPTY_CELL) {
            i++;
        }
        if (i > dimension) {
            Board upperNeighbor = new Board(this);
            swap(upperNeighbor.blocks, i, i - dimension);
            boardQueue.enqueue(upperNeighbor);
        }
        if (i < dimension * (dimension - 1) + 1) {
            Board lowerNeighbor = new Board(this);
            swap(lowerNeighbor.blocks, i, i + dimension);
            boardQueue.enqueue(lowerNeighbor);
        }
        if (i % dimension != OFFSET) {
            Board leftNeighbor = new Board(this);
            swap(leftNeighbor.blocks, i, i - 1);
            boardQueue.enqueue(leftNeighbor);
        }
        if (i % dimension != OFFSET - 1) {
            Board rightNeighbor = new Board(this);
            swap(rightNeighbor.blocks, i, i + 1);
            boardQueue.enqueue(rightNeighbor);
        }
        return boardQueue;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.valueOf(dimension)).append("\n");
        for (int i = 1; i < dimension * dimension + 1; i++) {
            builder.append((int) blocks[i]);
            if (i % dimension == OFFSET - 1) {
                builder.append('\n');
            } else {
                builder.append(' ');
            }
        }
        return builder.toString();
    }

    private void swap(char[] array, int i, int j) {
        char swap = array[i];
        array[i] = array[j];
        array[j] = swap;
    }
}
