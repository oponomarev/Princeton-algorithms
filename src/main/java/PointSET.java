import edu.princeton.cs.algs4.Point2D;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.RectHV;
import edu.princeton.cs.algs4.StdDraw;

import java.util.Set;
import java.util.TreeSet;

/**
 * @author Oleg Ponomarev
 * @since 2/20/17
 */
public class PointSET {
    private static final double BIAS = 0.000001;
    private Set<Point2D> set;

    public PointSET() {
        set = new TreeSet<>();
    }

    public boolean isEmpty() {
        return set.isEmpty();
    }

    public int size() {
        return set.size();
    }

    public void insert(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }
        set.add(p);
    }

    public boolean contains(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }
        return set.contains(p);
    }

    public void draw() {
        StdDraw.setPenRadius(0.01);
        for (Point2D point : set) {
            point.draw();
        }
    }

    public Iterable<Point2D> range(RectHV rect) {
        if (rect == null) {
            throw new NullPointerException();
        }
        Queue<Point2D> queue = new Queue<>();
        for (Point2D point : set) {
            if (rect.contains(point)) {
                queue.enqueue(point);
            }
        }
        return queue;
    }

    public Point2D nearest(Point2D p) {
        if (p == null) {
            throw new NullPointerException();
        }
        if (isEmpty()) {
            return null;
        }
        double distance = Double.POSITIVE_INFINITY;
        Point2D nearest = null;
        for (Point2D point : set) {
            if (point.distanceTo(p) - distance < BIAS) {
                distance = point.distanceTo(p);
                nearest = point;
            }
        }
        return nearest;
    }
}
