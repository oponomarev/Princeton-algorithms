package wordnet;

import edu.princeton.cs.algs4.BreadthFirstDirectedPaths;
import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.In;

public class SAP {
    private final Digraph digraph;

    public SAP(Digraph G) {
        if (G == null) {
            throw new IllegalArgumentException();
        }
        this.digraph = new Digraph(G);
    }

    private Ancestor getAncestor(Iterable<Integer> v, Iterable<Integer> w) {
        if (v == null || w == null) {
            throw new NullPointerException();
        }
        for (int vertex : v) {
            checkVertex(vertex);
        }
        for (int vertex : w) {
            checkVertex(vertex);
        }
        BreadthFirstDirectedPaths bfs1 = new BreadthFirstDirectedPaths(digraph, v);
        BreadthFirstDirectedPaths bfs2 = new BreadthFirstDirectedPaths(digraph, w);
        int len = Integer.MAX_VALUE;
        int ancestor = -1;
        for (int i = 0; i < digraph.V(); i++) {
            int sum;
            if (bfs1.hasPathTo(i)
                    && bfs2.hasPathTo(i)
                    && (sum = bfs1.distTo(i) + bfs2.distTo(i)) < len) {
                len = sum;
                ancestor = i;
            }
        }

        return len < Integer.MAX_VALUE ? new Ancestor(len, ancestor) : new Ancestor(-1, -1);
    }

    private void checkVertex(int v) {
        if (v < 0 || v > digraph.V() - 1) {
            throw new IndexOutOfBoundsException();
        }
    }

    private Ancestor getAncestor(int v, int w) {
        checkVertex(v);
        checkVertex(w);
        if (v == w) {
            return new Ancestor(0, v);
        }
        BreadthFirstDirectedPaths bfs1 = new BreadthFirstDirectedPaths(digraph, v);
        BreadthFirstDirectedPaths bfs2 = new BreadthFirstDirectedPaths(digraph, w);
        int len = Integer.MAX_VALUE;
        int ancestor = -1;
        for (int i = 0; i < digraph.V(); i++) {
            int sum;
            if (bfs1.hasPathTo(i)
                    && bfs2.hasPathTo(i)
                    && (sum = bfs1.distTo(i) + bfs2.distTo(i)) < len) {
                len = sum;
                ancestor = i;
            }
        }

        return len < Integer.MAX_VALUE ? new Ancestor(len, ancestor) : new Ancestor(-1, -1);
    }

    // length of shortest ancestral path between v and w; -1 if no such path
    public int length(int v, int w) {
        return getAncestor(v, w).length;
    }

    // a common ancestor of v and w that participates in a shortest ancestral path; -1 if no such path
    public int ancestor(int v, int w) {
        return getAncestor(v, w).vertex;
    }

    // length of shortest ancestral path between any vertex in v and any vertex in w; -1 if no such path
    public int length(Iterable<Integer> v, Iterable<Integer> w) {
        return getAncestor(v, w).length;
    }

    // a common ancestor that participates in shortest ancestral path; -1 if no such path
    public int ancestor(Iterable<Integer> v, Iterable<Integer> w) {
        return getAncestor(v, w).vertex;
    }

    public static void main(String[] args) {
        SAP sap = new SAP(new Digraph(new In(
                "src/test/resources/wordnet/digraph1.txt"
        )));
        System.out.println(sap.ancestor(1, 5));
        System.out.println(sap.length(1, 5));
    }

    private class Ancestor {
        private final int length;
        private final int vertex;

        public Ancestor(int length, int vertex) {
            this.length = length;
            this.vertex = vertex;
        }
    }
}
