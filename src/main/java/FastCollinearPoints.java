import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author Oleg Ponomarev
 * @since 24.01.2017
 */
public class FastCollinearPoints {
    private static final double PRECISION = 0.001;
    private List<LineSegment> lineSegmentList = new ArrayList<>();

    public FastCollinearPoints(Point[] points) {
        if (points == null) {
            throw new NullPointerException();
        }
        for (Point point : points) {
            if (point == null) {
                throw new NullPointerException();
            }
        }
        Arrays.sort(points);
        for (int i = 0; i < points.length; i++) {
            for (int j = i + 1; j < points.length; j++) {
                if (points[i].compareTo(points[j]) == 0) {
                    throw new IllegalArgumentException();
                }
            }
        }
        for (int outer = 0; outer < points.length; outer++) {
            Arrays.sort(points, outer, points.length, points[outer].slopeOrder());
            int count = 1;
            double currentSlope = points[outer].slopeTo(points[outer]);
            Point beginOfLineSegment = points[outer];
            Point endOfLineSegment = points[outer];
            boolean thresholdReached = false;
            for (int inner = outer + 1; inner < points.length; inner++) {
                if (Math.abs(points[outer].slopeTo(points[inner]) - currentSlope) < PRECISION) {
                    if (points[inner].compareTo(endOfLineSegment) > 0) {
                        endOfLineSegment = points[inner];
                    }
                    if (points[inner].compareTo(beginOfLineSegment) < 0) {
                        beginOfLineSegment = points[inner];
                    }
                    count++;
                    if (count > 3) {
                        thresholdReached = true;
                    }
                } else {
                    if (thresholdReached) {
                        lineSegmentList.add(new LineSegment(beginOfLineSegment, endOfLineSegment));
                    }
                    count = 2;
                    currentSlope = points[outer].slopeTo(points[inner]);
                    endOfLineSegment = points[outer].compareTo(points[inner]) > 0 ? points[outer] : points[inner];
                    beginOfLineSegment = points[outer].compareTo(points[inner]) < 0 ? points[outer] : points[inner];
                    thresholdReached = false;
                }
            }
            if (thresholdReached) {
                lineSegmentList.add(new LineSegment(beginOfLineSegment, endOfLineSegment));
            }
        }
    }

    public int numberOfSegments() {
        return lineSegmentList.size();
    }

    public LineSegment[] segments() {
        LineSegment[] array = new LineSegment[numberOfSegments()];
        return lineSegmentList.toArray(array);
    }
}
