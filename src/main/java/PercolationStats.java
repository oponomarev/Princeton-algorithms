import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;
import edu.princeton.cs.algs4.StdStats;

/**
 * @Author Oleg Ponomarev
 * @since 16.01.2017
 */
public class PercolationStats {
    private double[] results;
    private int trials;

    public PercolationStats (int n, int trials) {
        if (n <= 0 || trials <= 0) {
            throw new IllegalArgumentException();
        }
        this.trials = trials;
        results = new double[trials];
        Percolation[] percolations = new Percolation[trials];
        for (int index = 0; index < trials; index++) {
            percolations[index] = new Percolation(n);
            results[index] = performSimulation(percolations[index], n);
        }
    }

    private double performSimulation (Percolation percolation, int size) {
        while (!percolation.percolates()) {
            percolation.open(StdRandom.uniform(1, size + 1), StdRandom.uniform(1, size + 1));
        }
        return (percolation.numberOfOpenSites() * 1.0 / (size * size));
    }

    public double mean () {
        return StdStats.mean(results);
    }

    public double stddev () {
        return StdStats.stddev(results);
    }

    public double confidenceLo () {
        return (mean() - 1.96 * stddev() / Math.sqrt(trials));
    }

    public double confidenceHi () {
        return (mean() + 1.96 * stddev() / Math.sqrt(trials));
    }

    public static void main (String[] args) {
        int n = StdIn.readInt();
        int trials = StdIn.readInt();
        PercolationStats stats = new PercolationStats(n, trials);
        StdOut.println("mean                    = " + stats.mean());
        StdOut.println("stddev                  = " + stats.stddev());
        StdOut.println("95% confidence interval = " + stats.confidenceLo() + ", " + stats.confidenceHi());
    }
}
