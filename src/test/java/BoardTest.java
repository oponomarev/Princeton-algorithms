import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Oleg Ponomarev
 * @since 08.02.2017
 */
public class BoardTest {
    private Board testBoard;

    @Before
    public void setUp() throws Exception {
        int[][] input = {
                {8, 1, 3},
                {4, 2, 0},
                {7, 6, 5}
        };
        testBoard = new Board(input);
    }

    @Test
    public void dimension() throws Exception {
        assertEquals(3, testBoard.dimension());
    }

    @Test
    public void hamming() throws Exception {
        assertEquals(5, testBoard.hamming());
    }

    @Test
    public void manhattan() throws Exception {
        assertEquals(9, testBoard.manhattan());
    }

    @Test
    public void manhattan1() throws Exception {
        int[][] input = {
                {5, 8, 7},
                {1, 4, 6},
                {3, 0, 2}
        };
        Board goalBoard = new Board(input);
        assertEquals(17, goalBoard.manhattan());
    }

    @Test
    public void manhattan2() throws Exception {
        int[][] input = {
                {1, 0},
                {2, 3}
        };
        Board goalBoard = new Board(input);
        assertEquals(3, goalBoard.manhattan());
    }
    @Test
    public void manhattan3() throws Exception {
        int[][] input = {
                {2, 3},
                {0, 1}
        };
        Board goalBoard = new Board(input);
        assertEquals(5, goalBoard.manhattan());
    }


    @Test
    public void isGoal() throws Exception {
        assertFalse(testBoard.isGoal());
        int[][] goal = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 0}
        };
        Board goalBoard = new Board(goal);
        assertTrue(goalBoard.isGoal());
    }

    @Test
    public void twin() throws Exception {
        int[][] twin = {
                {1, 8, 3},
                {4, 2, 0},
                {7, 6, 5}
        };
        Board twinBoard = new Board(twin);
        assertEquals(testBoard.twin(), twinBoard);
    }

    @Test
    public void neighbors() throws Exception {
        Iterable<Board> boards = testBoard.neighbors();
        int count = 0;
        for (Board board : boards) {
            count++;
            System.out.println(board);
        }
        assertEquals(3, count);
    }

    @Test
    public void doNotEnqueuePreviousSearchNode() {
        int[][] prev = {
                {8, 1, 3},
                {4, 0, 2},
                {7, 6, 5}
        };
        int[][] searchNodeArray = {
                {8, 1, 3},
                {4, 2, 0},
                {7, 6, 5}
        };
        Board previous = new Board(prev);
        Board searchNode = null;
        for (Board board : previous.neighbors()) {
            if (board.equals(new Board(searchNodeArray))) {
                searchNode = board;
            }
        }

        int count = 0;
        for (Board board : searchNode.neighbors()) {
            count++;
            System.out.println(board);
        }
        assertEquals(2, count);
    }
}