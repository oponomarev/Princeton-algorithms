import edu.princeton.cs.algs4.Point2D;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

/**
 * @author Oleg Ponomarev
 * @since 22.02.2017
 */
public class KdTreeTest {
    @Test
    public void insert() throws Exception {
        KdTree kdTree = new KdTree();
        kdTree.insert(new Point2D(0, 0));
        kdTree.insert(new Point2D(0, 0));
        kdTree.insert(new Point2D(2, -1));
        kdTree.insert(new Point2D(-1, 1));
        kdTree.insert(new Point2D(-2, -3));
        kdTree.insert(new Point2D(-3, 0));
        kdTree.insert(new Point2D(-0.5, 3));
        kdTree.insert(new Point2D(0.5, -1.5));
        assertFalse(kdTree.size() == 0);
    }

}