import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author Oleg Ponomarev
 * @since 21.01.2017
 */
public class RandomizedQueueTest {
    static RandomizedQueue<String> queue;

    @Before
    public void init() {
        queue = new RandomizedQueue <>();
        queue.enqueue("1");
        queue.enqueue("2");
        queue.enqueue("3");
        queue.enqueue("4");
    }
    @Test
    public void isEmpty () throws Exception {
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        Assert.assertTrue(queue.isEmpty());
    }

    @Test
    public void differentIterators () {
        int count1 = 0;
        int count2 = 0;
        for (String s : queue) {
            count1++;
            System.out.print(s + " ");
        }
        System.out.println();
        for (String s : queue) {
            count2++;
            System.out.print(s + " ");
        }
        assertEquals(queue.size(), count1);
        assertEquals(queue.size(), count2);

    }

}