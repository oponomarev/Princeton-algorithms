import edu.princeton.cs.algs4.In;
import org.junit.Test;

/**
 * @Author Oleg Ponomarev
 * @since 11.02.2017
 */
public class SolverTest {
    private static final String PATH = "D:\\GoogleDisk\\IdeaProjects\\princeton-algs\\src\\test\\resources\\8puzzle\\puzzle3x3-31.txt";
    private static long start;
    static Solver solver;
    static  {
        In in = new In(PATH);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                blocks[i][j] = in.readInt();
        start  = System.nanoTime();
        solver = new Solver(new Board(blocks));
    }

    @Test
    public void isSolvable() throws Exception {
        System.out.println(solver.isSolvable());
        System.out.println("time is " + (System.nanoTime() - start) / 1_000_000 + " ms");
    }

    @Test
    public void moves() throws Exception {
        System.out.println(solver.moves());
    }

    @Test
    public void solution() throws Exception {

    }

}